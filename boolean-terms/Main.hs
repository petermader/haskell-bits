module Main where

import Prelude hiding (True, False, and, or, not)

data Id = X' | Y' | Z'

data B = T | F

instance Show B where
    show T = "T"
    show F = "F"

sheffer :: B -> B -> B
sheffer T T = F
sheffer _ _ = T

type Env = Id -> B

data WBool =
      True
    | False
    | Id Id
    | Sheffer WBool WBool

not a = a `Sheffer` a
a `or`    b = (not a) `Sheffer` (not b)
a `and`   b = not $ (not a) `or` (not b)
a `impl`  b = (not a) `or` b
a `equiv` b = (a `impl` b) `and` (b `impl` a)
a `xor`   b = not (a `equiv` b)

interpret :: WBool -> Env -> B
interpret True  _ = T
interpret False _ = F
interpret (Id id) β = β id
interpret (Sheffer t1 t2) β = (interpret t1 β) `sheffer` (interpret t2 β)

myβ :: Id -> B
myβ X' = T
myβ Y' = T
myβ Z' = T

x0 = Id X'
x1 = Id Y'
x2 = Id Z'

f = (x1 `or` not x0) `and` not ((not x1 `or` x0) `or` ((x2 `or` not x0) `and` (x2 `xor` not x1)))

main :: IO ()
main = putStrLn . show $ interpret f myβ
