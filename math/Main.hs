module Math where

import Prelude (Show, Bool(..))

infixl 1 &&
infixl 1 ||
infixl 1 <=
infixl 1 >=
infixl 1 <
infixl 1 >
infixl 2 ==
infixl 2 /=
infixl 3 +
infixl 3 -
infixl 4 *
infixl 4 /

id :: a -> a
id x = x

not :: Bool -> Bool
not True  = False
not False = True

(&&) :: Bool -> Bool -> Bool
True && True = True
_    && _    = False

(||) :: Bool -> Bool -> Bool
False || False = False
_     || _     = True

class Eq a where
    (==) :: a -> a -> Bool

(/=) :: Eq a => a -> a -> Bool
x /= y = not (x == y)

class (Eq a) => Ord a where
    (<=) :: a -> a -> Bool

(<) :: Ord a => a -> a -> Bool
x < y = x <= y && x /= y

(>) :: Ord a => a -> a -> Bool
x > y = not (x <= y)

(>=) :: Ord a => a -> a -> Bool
x >= y = not (x < y)

max :: Ord a => a -> a -> a
max a b = if a < b then b else a

class Simplifiable a where
    simplify :: a -> a

class Semigroup a where
    (+) :: a -> a -> a

class (Semigroup a) => Monoid a where
    zero :: a

class (Monoid a) => Group a where
    minus :: a -> a

(-) :: Group a => a -> a -> a
a - b = a + (minus b)

class (Group a) => Rng a where
    (*) :: a -> a -> a

class (Rng a) => Ring a where
    one :: a

class (Ring a) => Field a where
    inv :: a -> a

(/) :: Field a => a -> a -> a
a / b = a * (inv b)

data Nat = Z | S Nat deriving (Show)

instance Eq Nat where
    Z == Z = True
    Z == S x = False
    S x == Z = False
    S x == S y = x == y

instance Ord Nat where
    Z     <= Z     = True
    (S x) <= Z     = False
    Z     <= (S _) = False
    (S x) <= (S y) = x <= y

natAdd :: Nat -> Nat -> Nat
x `natAdd` Z   = x
x `natAdd` S y = S x `natAdd` y

natSub :: Nat -> Nat -> Nat
x     `natSub` Z     = x
Z     `natSub` y     = y
(S x) `natSub` (S y) = x `natSub` y

instance Semigroup Nat where
    (+) = natAdd

instance Monoid Nat where
    zero = Z

natMul :: Nat -> Nat -> Nat
x `natMul` Z   = Z
x `natMul` S Z = x
x `natMul` S y = x + y `natMul` x

data Int = Int Nat Nat deriving (Show)

instance Eq Int where
    (Int a b) == (Int c d) = a + d == b + c

instance Simplifiable Int where
    simplify (Int (S x) (S y)) = simplify (Int x y)
    simplify x = x

instance Ord Int where
    Int a b <= Int c d = a + d <= b + c

intAdd :: Int -> Int -> Int
(Int a b) `intAdd` (Int c d) = Int (a + c) (b + d)

instance Semigroup Int where
    (+) = intAdd

instance Monoid Int where
    zero = Int Z Z

intMul :: Int -> Int -> Int
(Int a b) `intMul` (Int c d) = Int (a `natMul` c + b `natMul` d) (a `natMul` d + b `natMul` c)

instance Group Int where
    minus (Int a b) = Int b a

instance Rng Int where
    (*) = intMul

instance Ring Int where
    one = Int (S Z) Z

data Rat = Rat Int Int deriving (Show)

instance Eq Rat where
    Rat a b == Rat c d = (a * d) == (b * c)

instance Simplifiable Rat where
    simplify (Rat a b) = Rat (simplify a) (simplify b)

instance Ord Rat where
    Rat a b <= Rat c d = (a * d) <= (b * c)

ratAdd :: Rat -> Rat -> Rat
Rat a b `ratAdd` Rat c d = Rat (a * d + b * c) (b * d)

instance Semigroup Rat where
    (+) = ratAdd

instance Monoid Rat where
    zero = Rat zero one

ratMul :: Rat -> Rat -> Rat
Rat a b `ratMul` Rat c d = Rat (a * c) (b * d)

instance Group Rat where
    minus (Rat a b) = Rat (minus a) b

instance Rng Rat where
    (*) = ratMul

instance Ring Rat where
    one = Rat one one

instance Field Rat where
    inv (Rat a b) = Rat b a

data Polynomial a = Nil | Cons a (Polynomial a) deriving (Show)

data Degree = ZeroDegree | Degree Nat

instance Eq Degree where
    ZeroDegree == ZeroDegree = True
    Degree x   == Degree y   = x == y

instance Ord Degree where
    ZeroDegree <= ZeroDegree = True
    Degree x   <= Degree y   = x <= y

deg :: Polynomial a -> Degree
deg Nil = ZeroDegree
deg (Cons a Nil) = Degree Z
deg (Cons a p)   = Degree (S x) where (Degree x) = deg p

extend :: (Ring a) => Nat -> Polynomial a -> Polynomial a
extend Z p = p
extend (S x) p = Cons zero (extend x p)

equalizeLengths :: (Ring a) => Polynomial a -> Polynomial a -> (Polynomial a, Polynomial a)
equalizeLengths Nil Nil = (Nil, Nil)
equalizeLengths p@(Cons _ _) Nil = (p, extend a Nil) where (Degree a) = deg p
equalizeLengths Nil q@(Cons _ _) = (extend b Nil, q) where (Degree b) = deg q
equalizeLengths p q = if a < b then (extend (b `natSub` a) p, q)
                      else if a > b then (p, extend (a `natSub` b) q)
                      else (p, q)
                      where (Degree a) = deg p
                            (Degree b) = deg q

polyAdd' :: (Ring a) => Polynomial a -> Polynomial a -> Polynomial a
polyAdd' Nil Nil = Nil
polyAdd' (Cons a p) (Cons b q) = Cons (a + b) (polyAdd' p q)

polyAdd :: (Ring a) => Polynomial a -> Polynomial a -> Polynomial a
polyAdd p q = polyAdd' p' q' where (p', q') = equalizeLengths p q

instance (Ring a) => Semigroup (Polynomial a) where
    (+) = polyAdd

instance (Ring a) => Monoid (Polynomial a) where
    zero = Nil

instance (Ring a) => Group (Polynomial a) where
    minus Nil = Nil
    minus (Cons a p) = Cons (minus a) (minus p)


polyMul :: (Ring a) => Polynomial a -> Polynomial a -> Polynomial a

----------------------------- TESTS -----------------------------

threeNat :: Nat
threeNat = S (S (S Z))

fiveNat :: Nat
fiveNat = S (S (S (S (S Z))))

eightNat :: Nat
eightNat = threeNat + fiveNat

minusTwo :: Int
minusTwo = (Int threeNat (S Z)) - (Int fiveNat (S Z))

four :: Int
four = minusTwo * minusTwo

oneFourth :: Rat
oneFourth = Rat one four

four' :: Rat
four' = one / oneFourth

squared :: Polynomial Int
squared = Cons one (Cons zero (Cons zero Nil))
