# haskell-bits

A collection of little Haskell programs:

* `boolean-terms`: an interpreter for Boolean terms (to be extended to a
  minimal proof checker)
* `probability-game`: a simulation of a probability problem
* `math`: an implementation of math in Haskell
