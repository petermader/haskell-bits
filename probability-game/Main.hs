module Main where

import Control.Monad.Trans.State
import System.Random

getRandomNumber :: State StdGen Float
getRandomNumber = state random

simulateGame :: State StdGen Float
simulateGame = do
    x1 <- getRandomNumber
    x2 <- getRandomNumber
    x3 <- getRandomNumber
    return $ maximum [x1, x2, x3]

doGames :: Int -> State StdGen Float
doGames 0 = return 0
doGames n = do
    x <- simulateGame
    y <- doGames $ n - 1
    return $ x + y

main = do
    let passes = 1000
    gen <- getStdGen
    let val = evalState (doGames passes) gen
    putStrLn . show $ val / (fromIntegral passes)
