module LambdaCalculus where

import Prelude hiding ((.), succ, id, const)

data Id = A | B | C | F | G | H | N | M | X | Y | Z deriving (Eq, Show)

data Expression
    = Var Id
    | Abstraction Id Expression
    | Application Expression Expression
    deriving (Show)

α :: Expression -> Id -> Expression
α (Var _) y     = Var y
α a@(Abstraction x t) y
    | x == y    = a
    | otherwise = Abstraction y $ substitute t x (Var y)

substitute :: Expression -> Id -> Expression -> Expression
substitute (Var x) y t
    | x == y    = t
    | otherwise = Var x
substitute a@(Abstraction x s) y t
    | x == y    = a
    | otherwise = Abstraction x $ substitute s y t
substitute (Application x s) y t = Application (substitute x y t) (substitute s y t)

interpret :: Expression -> Expression
interpret a@(Var _)           = a
interpret a@(Abstraction _ _) = a
interpret (Application f x)
    | (Abstraction y t) <- interpret f = interpret $ substitute t y $ interpret x
    | otherwise = error $ "You can't call a Var or an Application: " ++ show f ++ " " ++ show x

-- syntactic sugar for lambda expressions
λ   = Abstraction

infixl 1 .
(.) = Application

infixr 0 .:
(.:) = ($)

tru = λ X (λ Y (Var X))
fls = λ X (λ Y (Var Y))

id = λ X .: Var X
const = λ X .: λ Y .: Var X

zero = λ F (λ X (Var X))
one  = λ F (λ X (Var F . Var X))
two  = λ F (λ X (Var F . (Var F . Var X)))

succ = λ N .: λ F .: λ X (Var F . (Var N . Var F . Var X))
pred = λ N .: λ F .: λ X (Var N . (λ G .: λ H .: Var H . (Var G . Var F)) . (const . Var X) . id)
